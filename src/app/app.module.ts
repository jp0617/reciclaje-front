import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReciclajeComponent } from './components/reciclaje/reciclaje.component';
import { NgChartsModule } from 'ng2-charts';
import { CrearReciclajeComponent } from './components/crear-reciclaje/crear-reciclaje.component';
import { EditarReciclajeComponent } from './components/editar-reciclaje/editar-reciclaje.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ReciclajeComponent,
    CrearReciclajeComponent,
    EditarReciclajeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
