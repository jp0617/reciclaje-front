import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearReciclajeComponent } from './crear-reciclaje.component';

describe('CrearReciclajeComponent', () => {
  let component: CrearReciclajeComponent;
  let fixture: ComponentFixture<CrearReciclajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearReciclajeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearReciclajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
