import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ReciclajeService } from 'src/app/services/reciclaje/reciclaje.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-reciclaje',
  templateUrl: './crear-reciclaje.component.html',
  styleUrls: ['./crear-reciclaje.component.css'],
})
export class CrearReciclajeComponent implements OnInit {
  grupos: any = [
    { value: 1, nombre: '1°1' },
    { value: 2, nombre: '1°2' },
    { value: 3, nombre: '1°3' },
    { value: 4, nombre: '1°4' },
    { value: 5, nombre: '1°5' },
    { value: 6, nombre: '2°2' },
    { value: 7, nombre: '2°3' },
    { value: 8, nombre: '2°4' },
    { value: 9, nombre: '3°1' },
    { value: 10, nombre: '4°1' },
    { value: 11, nombre: '4°3' },
  ];

  tipor: any = [
    { value: 1, nombre: 'Carton liso' },
    { value: 2, nombre: 'Vidrio' },
    { value: 3, nombre: 'Chatarra o metal' },
    { value: 4, nombre: 'plega' },
    { value: 5, nombre: 'Plasticos' },
    { value: 6, nombre: 'tapas' },
    { value: 7, nombre: 'pasta' },
    { value: 8, nombre: 'archivo' },
    { value: 9, nombre: 'revistas' },
    { value: 10, nombre: 'pasta negra' },
  ];

  constructor(
    private formbuilde: FormBuilder,
    private recicles: ReciclajeService,
    private route: Router
  ) {}

  ngOnInit(): void {}

  reciclaje = this.formbuilde.group({
    cantidad: 0,
    id_tipo_rec: 0,
    id_grupo: 0,
    etapa: 0,
  });

  info = async () => {
    this.recicles.creater(this.reciclaje.value).subscribe(
      (res: any) => {
        Swal.fire({
          text: 'Muchas gracias',
        }).then(() => {
          this.route.navigate(['/home']);
        });
      },
      (err) => {
        console.log(err);
      }
    );
  };
}
