import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private formbuilde: FormBuilder,private authservices:AuthService,private route:Router) { }

  ngOnInit(): void {
  }

  login = this.formbuilde.group({
    doc: [''],
    password: [''],

  })

  loginfo = () => {
    this.authservices.loginservices(this.login.value).subscribe(
      (res: any) => {
        sessionStorage.setItem("rol", res.id_tipo_user)
        sessionStorage.setItem('user', res.id_persona);
        this.route.navigate(['home'])
      },
      (err: any) => {
        console.log(err)
      }
    )
    
  }
}
