import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommentsService } from 'src/app/services/comment/comments.service';
import { ReciclajeService } from 'src/app/services/reciclaje/reciclaje.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  rol = sessionStorage.getItem('rol') == '1' ? true : false;
  reciclajes: any = [];

  constructor(private recicles: ReciclajeService, private route: Router) {}

  ngOnInit(): void {
    this.getreciclaje(1);
  }

  npublicacion = async () => {
    const { value: pub } = await Swal.fire({
      input: 'text',
      inputPlaceholder: '¿Qué tienes de interesante?',
    });

    const data = {
      texto: pub,
      id_user: sessionStorage.getItem('user'),
    };
  };

  getreciclaje = (id: Number) => {
    this.recicles.getall(id).subscribe(
      (res: any) => {
        this.reciclajes = res;
      },
      (err) => {
        console.log(err);
      }
    );
  };

  etreciclaje = (id: Number) => {
    this.route.navigate([`/ediciclaje/${id}`]);
  };
  detreciclaje = (id: Number) => {
    Swal.fire({
      title: '¿Seguro que quieres eliminar este registro?',
      showDenyButton: true,
      confirmButtonText: 'Sí',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.recicles.delete(id).subscribe(
          (res: any) => {
            Swal.fire({
              icon: 'success',
              text: 'Se elimino correctamente',
            }).then(() => {
              this.getreciclaje(1);
            });
          },
          (err) => {
            console.log(err);
          }
        );
      }
    });
  };
}
