import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarReciclajeComponent } from './editar-reciclaje.component';

describe('EditarReciclajeComponent', () => {
  let component: EditarReciclajeComponent;
  let fixture: ComponentFixture<EditarReciclajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarReciclajeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditarReciclajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
