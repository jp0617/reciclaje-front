import { Component, OnInit } from '@angular/core';
import { ReciclajeService } from 'src/app/services/reciclaje/reciclaje.service';
import { ChartOptions, ChartType, ChartDataset } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-annotation';

@Component({
  selector: 'app-reciclaje',
  templateUrl: './reciclaje.component.html',
  styleUrls: ['./reciclaje.component.css'],
})
export class ReciclajeComponent implements OnInit {
  etapa: Number = 1;
  grupo: String = '1°1';

  grupos: any = [
    { value: 1, nombre: '1°1' },
    { value: 2, nombre: '1°2' },
    { value: 3, nombre: '1°3' },
    { value: 4, nombre: '1°4' },
    { value: 5, nombre: '1°5' },
    { value: 6, nombre: '2°2' },
    { value: 7, nombre: '2°3' },
    { value: 8, nombre: '2°4' },
    { value: 9, nombre: '3°1' },
    { value: 10, nombre: '4°1' },
    { value: 11, nombre: '4°3' },
  ];

  etapas: any = [
    { value: 1, nombre: '1' },
    { value: 2, nombre: '2' },
    { value: 3, nombre: '3' },
  ];

  reciclajes: any = [
    {
      id: 1,
      cantidad: '3',
      id_tipo_rec: 2,
      id_grupo: 1,
      etapa: 1,
      nombre: '1°1',
      tipo_rec: 'Vidrio',
    },
  ];
  Label: [] = [];

  public Color: [] = [];

  constructor(private recicles: ReciclajeService) {}

  ngOnInit(): void {
    this.getreciclaje('1°1', 1);
  }

  etapaonchange = (id: any) => {
    console.log(
      '🚀 ~ file: reciclaje.component.ts ~ line 57 ~ ReciclajeComponent ~ id',
      id
    );
    this.etapa = id;
    this.getreciclaje(this.grupo, this.etapa);
  };
  grupoonchange = (id: any) => {
    this.grupo = id;
    this.getreciclaje(this.grupo, this.etapa);
  };

  getreciclaje = (id: any, etapa: any) => {
    this.recicles.getbygrade(id, etapa).subscribe(
      (res: any) => {
        this.reciclajes = res;
        this.cargarDatos(this.reciclajes, this.reciclajes);
      },
      (err) => {
        console.log(err);
      }
    );
  };

  getreciclajebyetapa = ( etapa: any) => {
    this.recicles.getall(etapa).subscribe(
      (res: any) => {
        this.reciclajes = res;
        this.cargarDatos(this.reciclajes, this.reciclajes);
      },
      (err) => {
        console.log(err);
      }
    );
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: {}, yAxes: {} },
  };
  public barChartLabels: any = [
    'Carton liso',
    'Vidrio',
    'Chatarra o metal',
    'Plega',
    'Plasticos',
    'tapas',
    'Pasta',
    'Archivo',
    'Revistas',
    'Pasta negra',
  ];
  //Tipo de grafico que queremos: ejem: line, bar, radar
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];
  //Datos que vamos a cargar en las graficas
  public barChartData: any = [];
  public chartColors: any;

  private categoria: any;
  private dato!: string;
  //Arreglo de los datos que vamos a pasar
  private datos = [];
  //Arreglo de las categorias que vamos a pasar
  private nombreCategoria = [];
  //Arreglo de los colores que vamos a pasar
  private colores = [];

  //Función que carga los datos en la grafica, asi como los colores
  cargarDatos(datos: [], nombreCategoria: []) {
    
    this.barChartData = [];
    this.chartColors = [];

    for (const index in datos) {
      if (nombreCategoria[index]['id_tipo_rec'] == 1) {
        this.barChartData.push({
          data: [datos[index]['cantidad'], 0, 0, 0, 0, 0, 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 2) {
        this.barChartData.push({
          data: [0, datos[index]['cantidad'], 0, 0, 0, 0, 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 3) {
        this.barChartData.push({
          data: [0, 0, datos[index]['cantidad'], 0, 0, 0, 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 4) {
        this.barChartData.push({
          data: [0, 0, 0, datos[index]['cantidad'], 0, 0, 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 5) {
        this.barChartData.push({
          data: [0, 0, 0, 0, datos[index]['cantidad'], 0, 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 6) {
        this.barChartData.push({
          data: [0, 0, 0, 0, 0, datos[index]['cantidad'], 0, 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 7) {
        this.barChartData.push({
          data: [0, 0, 0, 0, 0, 0, datos[index]['cantidad'], 0, 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 8) {
        this.barChartData.push({
          data: [0, 0, 0, 0, 0, 0, 0, datos[index]['cantidad'], 0, 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 9) {
        this.barChartData.push({
          data: [0, 0, 0, 0, 0, 0, 0, 0, datos[index]['cantidad'], 0],
          label: nombreCategoria[index]['nombre'],
        });
      } else if (nombreCategoria[index]['id_tipo_rec'] == 10) {
        this.barChartData.push({
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, datos[index]['cantidad']],
          label: nombreCategoria[index]['nombre'],
        });
      }
    }
  }
}
