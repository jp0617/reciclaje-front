import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  api_uri = 'http://localhost:3000/pub';
  api_uri_com = 'http://localhost:3000/com';
  constructor(private http: HttpClient) {}

  pubservices = () => this.http.get(`${this.api_uri}`);

  postservices = (data: {}) => this.http.post(`${this.api_uri}`, data);

  postcomments = (data: {}) => this.http.post(`${this.api_uri_com}`, data);
}
