import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ReciclajeService {
  uri_back = `http://localhost:3000/reciclaje`;
  constructor(private http: HttpClient) {}

  getall = (etapa: Number) => this.http.get(`${this.uri_back}/?id=${etapa}`);
  getbyid = (id: Number) => this.http.get(`${this.uri_back}/id/${id}`);
  getbygrade = (id: Number, etapa: Number) =>
    this.http.get(`${this.uri_back}/grade/?id=${id}&etapa=${etapa}`);
  creater = (datos: {}) => this.http.post(`${this.uri_back}`, datos);
  updater = (datos: {}) => this.http.put(`${this.uri_back}`, datos);

  delete = (id: Number) => this.http.delete(`${this.uri_back}/?id=${id}`);
}
