import { TestBed } from '@angular/core/testing';

import { ReciclajeService } from './reciclaje.service';

describe('ReciclajeService', () => {
  let service: ReciclajeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReciclajeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
