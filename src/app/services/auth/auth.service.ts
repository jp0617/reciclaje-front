import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  api_uri = "http://localhost:3000/auth";
  constructor(private http: HttpClient) { }

  loginservices = (data:{}) => this.http.post(`${this.api_uri}/login`,data)
}
