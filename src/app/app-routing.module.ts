import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearReciclajeComponent } from './components/crear-reciclaje/crear-reciclaje.component';
import { EditarReciclajeComponent } from './components/editar-reciclaje/editar-reciclaje.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ReciclajeComponent } from './components/reciclaje/reciclaje.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/reciclaje',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'reciclaje',
    component: ReciclajeComponent,
  },
  {
    path: 'creciclaje',
    component: CrearReciclajeComponent,
  },
  {
    path: 'ediciclaje/:id',
    component: EditarReciclajeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
